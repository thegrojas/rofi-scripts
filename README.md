<p align="center">
  <a href="" rel="noopener">
</p>

<h1 align="center">Rofi Scripts</h1>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]() 
[![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](./LICENSE)

</div>

---

<p align="center">Search and Launch Scripts using Rofi.
    <br> 
</p>

<p align="center">
  <a href="#about">🧐 About</a> •
  <a href="#getting_started">🏁 Getting Started</a> •
  <a href="#usage">🤹 Usage</a> •
  <a href="#changelog">📜 Changelog</a> •
  <a href="#todo">✅ Todo</a> •
  <a href="#built_using">🔨 Built Using</a> •
  <a href="#contributing">⚙️ Contributing</a> •
  <a href="#credits">✍️ Credits</a> •
  <a href="#support">🤲 Support</a> •
  <a href="#license">⚖️ License</a>
</p>

<div align="center">

![Screenshot](./showcase.gif)

</div>

## 🧐 About <a name="about"></a>

Search and Launch Scripts using Rofi.

## 🏁 Getting Started <a name="getting_started"></a>

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### 🦺 Prerequisites

- Rofi
- Make

### 📦 Installing

#### 1. Clone the repository

Open a terminal and clone this repository to your computer using git:

```sh
git clone https://gitlab.com/thegrojas/rofi-scripts.git
```
Then `cd` into the cloned repo:

```sh
cd rofi-scripts
```

#### 2. Install using `make`

Just run the following command inside this repository to install the script, create basic config file and add some sample scripts.

```sh
make install
```

## 🤹 Usage <a name="usage"></a>

As an example copy the contents of the sample `scripts` directory in this repository to the default scripts folder on your computer:

```sh
cp -R scripts $HOME/.scripts
```

Then execute rofi-scripts. There are many ways to use rofi-scripts:

**From the terminal**

Kind of beats the whole idea but for testing just run type `rofi-scripts` in your terminal.

**Add a shortcut**

I use `SUPER + X` to launch rofi-scripts.

**From rofi**

Since this is nothing more than a script you could launch rofi, then type `rofi-scripts` and hit `ENTER`, after that you will see the launcher appear with your scripts ready to be launched.

## 📜 Changelog <a name="changelog"></a>

All notable changes to this project will be documented in `CHANGELOG.md`. Click [here](./CHANGELOG.md) to take a look.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## ✅ Todo <a name="todo"></a>

Pending tasks will be tracked in `TODO.md`. Click [here](./TODO.md) to take a look.

## 🔨 Built Using <a name="built_using"></a>

- [Rofi](https://github.com/davatorium/rofi) - Launcher

## ✍️ Credits <a name="credits"></a>

- Julian Thomé for technically writing this [script](https://github.com/davatorium/rofi/wiki/Script-Launcher)

See also the list of [contributors](#) who participated in this project.

## 🤲 Support <a name="support"></a>

For now, just by sharing and/or staring this repository you are supporting my work.

## ⚖️ License <a name="license"></a>

This code is released under the [GPLv3](./LICENSE) license. For more information refer to `LICENSE`

#!/bin/bash

# Saying "Hello" in the terminal.

echo "Hello"

# Saying "Hello" in the desktop.

notify-send "Hello"

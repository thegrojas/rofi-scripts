.DEFAULT_GOAL := help

.PHONY: help

help: ## Prints help for Makefile.
	@echo "These are the available targets for this Makefile: \n"
	@grep -E '^[a-zA-Z_-%]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'

install: ## Installs config file and binary.
	@echo "Installing..."
	@install -v -m 0755 rofi-scripts /usr/local/bin/
	@echo "Done!"

variable-%: ## Print Makefile variable %. Eg: make print-TARGET will print the contents of the variable TARGET.
	@echo $*=$($*)

